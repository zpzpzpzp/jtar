# Jtar

## 简介
> Jtar支持tar打包和tar解包功能。

## 效果展示
![avatar](screenshot/效果展示.gif)

## 下载安装
```shell
npm install @ohos/jtar --save
```
OpenHarmony npm环境配置等更多内容，请参考 [如何安装OpenHarmony npm包](https://gitee.com/openharmony-tpc/docs/blob/master/OpenHarmony_npm_usage.md) 。

## 使用说明

1. 在page页面引入包

 ```
   import jtar from 'libjtar.so';
   import {JTar} from "../jtar.ets"
 ```
2. 创建tartest对象

 ```
   let fileName: string = "tartest";
   let path: string = "tartestdir";
   let tartest: JTar = new JTar(fileName, path);
 ```
3. 初始化Tar或者Sys Tar测试环境，创建生成tartestdir目录，并且包含一些txt文件

 ```
   JtarTestInit.TestTarInit();
 ```
4. Tar或者Sys Tar功能测试，运行后会将环境中的tartestdir目录打包为tartest.tar文件

 ```
   tartest.tar();或者tartest.systar();
 ```
5. 初始化Untar或者Sys Untar测试环境

 ```
   JtarTestInit.TestUnTarInit();
 ```
6. Untar或者Sys Untar功能测试，运行后会将环境中的tartest.tar文件解包

 ```
   tartest.untar();或者tartest.sysuntar();
 ```

## 以上功能测试均可通过在设备应用目录中查看

在设备shell中:

1. 进入到应用files目录中

```
   # cd /data/app/el2/100/base/com.zh.jtar/haps/entry/files
```
2. 点击Tar Init，会生成tartestdir，tartestdir中包含三个txt文件

```
   # ls
   tartestdir
   # ls tartestdir/
   one.txt  three.txt  two.txt
```
3. 点击Tar或者Sys Tar，会打包生成对应的tartest.tar文件

```
   # ls
   tartest.tar  tartestdir
```
4. 点击UnTar Init，生成对应的tartest.tar文件

```
   # ls
   tartest.tar
```
5. 点击UnTar或者Sys UnTar，生成对应的tartest.tar文件的原始文件

```
   # ls
   tartest.tar  tartestdir
   # ls tartestdir
   one.txt  three.txt  two.txt
```

## 接口说明

1. 修改生成的tar文件名
   `setTarName(name: string)`
   
2. 修改解包目标的Untar文件名
   `setUnTarName(name: string)`
   
3. 修改解包目标的Untar路径
   `setUnTarPath(path: string)`

4. 添加一个需要tar的文件或目录
   `addTarPath(path: string)`

5. 删除一个需要tar的文件或目录
   `delUnTarPath(path: string)`

6. tar打包
   `tar(): number`

7. 调用系统接口进行打包
   `systar(): number`

8. untar解包
   `untar(path?: string): number`

9. 调用系统接口进行解包
   `sysuntar(path?: string): number`

## 兼容性
支持 OpenHarmony API version 8 及以上版本。

## 目录结构
````
|---- jtar  
|     |---- entry  # 示例代码文件夹
|     |---- jtar  # jtar库文件夹
|                 |---- cpp  # c++详细接口
|                 |---- ets
|                       |---- jtar.ets  # 接口封装
|           |---- index.ets  # 对外接口
|     |---- README.md  # 安装使用方法                    
````

## 贡献代码
使用过程中发现任何问题都可以提 [Issue](https://gitee.com/hihopeorg/jtar/issues) 给我们，当然，我们也非常欢迎你给我们发 [PR](https://gitee.com/hihopeorg/jtar/pulls) 。

## 开源协议
本项目基于 [Apache License 2.0](https://gitee.com/hihopeorg/jtar/blob/master/LICENSE) ，请自由地享受和参与开源。